<?php 
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE, HEAD");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
header('content-type: application/json; charset=utf-8');
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
	die();
}


$response	=	array(); 
$json 		= 	file_get_contents("php://input"); 
$objeto 	= 	json_decode($json);

$name = $objeto->name;
$email = $objeto->email;
$phone = $objeto->phone;
$token = $objeto->token;
$amount = $objeto->amount;
$currency = $objeto->currency;
$unique_id = $objeto->tp_id;
$invoice_num = $objeto->invoice_num;

if ($name != "" && $email != "" && $phone != "" && $token != "" && $amount != "" && $currency != "" && $unique_id != "" && $invoice_num != "") {

	$ch = curl_init();

	$data = array(
		"line_items" => [array(
			"name"=> "Curso 1",
			"unit_price"=> $amount,
			"quantity"=> 1
		)],
		"currency"=> $currency,
		"customer_info"=> array(
			"name"=> $name,
			"email"=> $email,
			"phone"=> $phone
		),
		"metadata"=> array("reference"=> $unique_id, "more_info"=> $invoice_num),
		"charges"=> [array(
			"payment_method" => array(
				"type"=> "card",
				"token_id"=> $token
			)
		)]
	);

	curl_setopt($ch, CURLOPT_URL, 'https://api.conekta.io/orders');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
	curl_setopt($ch, CURLOPT_USERPWD, 'key_qYreXmUGWbZBgTgsCo3wDw' . ':' . '');

	$headers = array();
	$headers[] = 'Accept: application/vnd.conekta-v2.0.0+json';
	$headers[] = 'Content-Type: application/json';
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	if (curl_errno($ch)) {
		$response["error"] = curl_error($ch);
	}

	$response["result"] = 1;
	$response["data"] = json_decode(curl_exec($ch));

	curl_close($ch);

} else {
	$response["result"] = 0;
	$response["data"] = $objeto;
	$response["error"] = "Faltan parametros";
}

print_r(json_encode($response));


?>