<?php 

//Inicialización del cliente Conekta a través de la adición de la llave privada y versión del API.
require_once("../conekta/lib/Conekta.php");
\Conekta\Conekta::setApiKey("key_xcYVazz6Jcby5pET3WidHA");
\Conekta\Conekta::setApiVersion("2.0.0");


$token = $_POST["conektaTokenId"];
$name = $_POST["name"];
$phone = $_POST["phone"];
$email = $_POST["email"];
$amount = $_POST["amount"] * 100;

try{
  $order = \Conekta\Order::create(
    [
        'line_items'=> [
            [
                'name'        => 'Curso Forex',
                'description' => 'Webinar Curso(s)',
                'unit_price'  => $amount,
                'quantity'    => 1,
                'sku'         => 'none',
                'category'    => 'Webinar',
                'tags'        => ['none']
            ]
        ],
        'currency' => 'usd',
        'charges'  => [
            [
                'payment_method' => [
                    'type'       => 'card',
                    "token_id" => $token
                ],
                'amount' => $amount,
            ]
        ],
        'currency' => 'mxn',
        'customer_info' => [
            'name'  => $name,
            'phone' => $phone,
            'email' => $email,
        ]]
  );
} catch (\Conekta\ProcessingError $error){
  echo $error->getMessage();
} catch (\Conekta\ParameterValidationError $error){
  echo $error->getMessage();
} catch (\Conekta\Handler $error){
  echo $error->getMessage();
}

// return 1;
?>