<?php

$headerToken    = $_POST["headerToken"];
$unitPrice    = $_POST["unitPrice"];
$description    = $_POST["description"];

// echo $headerToken;

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://www.feenicia.net/receipt/order/create",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS =>"{\"amount\": 1,\"items\": [{\"amount\": 1,\"description\": \"".$description."\",\"Id\": 0,\"Quantity\": \"1\",\"unitPrice\": \"".$unitPrice."\"}],\"merchant\": \"0000000000006038\",\"userId\": \"armando385\"}",
  CURLOPT_HTTPHEADER => array(
    "x-requested-with:" . $headerToken ,
    "Content-Type: application/json"
  ),
));

$response = curl_exec($curl);

curl_close($curl);

echo $response;


?>