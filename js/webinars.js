/*$(window).on('resize', function()
{
	location.reload();
});*/
$(document).ready(function()
{  
	var course = '<h2>El título del curso o webinar</h2><p class="p2">Aprenda a invertir de forma segura con nuestros cursos de formación. Desde nivel básico a nivel experto</p>';
	var selectedCourse = [1];
	var shoppingCar = 0;
	
	$(".aside3").slideUp();
	
	jQuery.datetimepicker.setLocale('es');
	jQuery('#filter-date').datetimepicker
	({
		format:'Y/m/d     H:i',
		validateOnBlur: false,
	});
	
	$('.icon2').click(function()
	{
		$('#filter-date').datetimepicker('show');
	});
	
	$('.course1').click(function()
	{
		$(".aside3").slideDown("fast");
		$(".aside5").hide();
		selectedCourse[0] = $('#course1').text();
		selectedCourse[1] = $('#price1').text();
	});
	
	$('.course2').click(function()
	{
		$(".aside3").slideDown("fast");
		$(".aside5").hide();
		selectedCourse[0] = $('#course2').text();
		selectedCourse[1] = $('#price2').text();
	});
	
	$('.aside5 .div5:nth-child(1) .button1').click(function()
	{
		selectedCourse[0] = $('#course1').text();
		selectedCourse[1] = $('#price1').text();
		processData(selectedCourse, shoppingCar, total);
		shoppingCar++;
		$(".button3").css("background", "#f0923c");
	});
	
	$('.aside5 .div5:nth-child(2) .button1').click(function()
	{
		if($('#filter-date').val() != "")
		{		
			selectedCourse[0] = $('#course2').text();
			selectedCourse[1] = $('#price2').text();
			processData(selectedCourse, shoppingCar, total);
			shoppingCar++;
			$(".button3").css("background", "#f0923c");
		}
		else
		{
			$("#advice").css("display", "flex");
			add = '<p class="error">Favor de elejir la fecha y horas que desea</p>';
			$('#advice').html(add);
			
			setTimeout(function()
			{
				$("#advice").css("display", "none");
			}, 2000);
		}
	});
	
	$('.aside3 .button1').click(function()
	{
		$(".aside5").slideDown("fast");
		$(".aside3").hide();
		processData(selectedCourse, shoppingCar, total);
		shoppingCar++;
		$(".button3").css("background", "#f0923c");
	});
	
	$('.button3').click(function()
	{
		if($("#total").text() != "$0")
		{
			var arrayDataCourses = [shoppingCar];
			var arrayDataPrices = [shoppingCar];
			
			for(i=0;i<shoppingCar;i++)
			{
				arrayDataCourses[i] = $("#shoppingCar"+i+" .td1").text();
				arrayDataPrices[i] = $("#shoppingCar"+i+" .td2").text();
			}
			$.cookie('course', JSON.stringify(arrayDataCourses));
			$.cookie('price', JSON.stringify(arrayDataPrices));
			$.cookie('date', $("#filter-date").val());
			$.cookie('total', $("#total").text());
			window.location.replace("Pago-seguro.html");
		}
	});
	
	$('.select1').change(function()
	{
		var hour = parseInt($('.select1').val());
		if(hour != 1)
		{
			var price = (hour - 1) * 200 + 1100;
			price = new Intl.NumberFormat("en-IN", {maximumSignificantDigits: 3}).format(price);
			$("#price2").html('$'+price);
		}
		else
		{
			$("#price2").html('$1100');
		}
	});
	
	$('.button4').click(function()
	{
		var courseData = [1];
		var priceData = [1];
		courseData[0] = $(this).attr("course");
		priceData[0] = $("#price2").text();
		if(courseData == "Sesión privada 1a1")
		{
			if($("#filter-date").val() != "")
			{
				$.cookie('course', courseData);
				$.cookie('price', priceData);
				$.cookie('total', $("#total").text());
				window.location.replace("Pago-seguro.html");
			}
			else
			{
				$("#advice").css("display", "flex");
				add = '<p class="error">Favor de elejir la fecha y horas que desea</p>';
				$('#advice').html(add);
				
				setTimeout(function()
				{
					$("#advice").css("display", "none");
				}, 2000);
			}
		}
		else
		{
			$.cookie('course', JSON.stringify(courseData));
			$.cookie('price', JSON.stringify(priceData));
			$.cookie('total', $("#price1").text());
			window.location.replace("Pago-seguro.html");
		}
	});
	
});

function processData(selectedCourse, shoppingCar, total)
{
	var b1 = 0;
	var allValue = $(".td1").map(function(){
               return $.trim($(this).text());
            }).get();
	for(i=0;i<allValue.length;i++)
	{
		if(allValue[i] == selectedCourse[0])
		{
			b1 = 1;
		}
	}	

	if(b1 == 0)
	{
		var add = '<tr id="shoppingCar'+shoppingCar+'">'+
					'<td class="td1">'+selectedCourse[0]+'</td>'+
					'<td class="td2">'+selectedCourse[1]+' <label>USD</label></td>'+
					'<td class="td3"><label class="icon-trash" id="trash'+shoppingCar+'"></label></td>'+
				'</tr>';
		$(".table1").append(add);
		
		getTotal();
		
		$('#trash'+shoppingCar).click(function()
		{
			$('#shoppingCar'+shoppingCar).remove();
			getTotal();
			if($("#total").text() == "$0")
			{
				$(".button3").css("background", "#f3c49a");
			}
		});
	}
}

function getTotal()
{
	var total = 0;
	var allValue = $(".td2").map(function(){
               return $.trim($(this).text());
            }).get();
			
	for(i=0;i<allValue.length;i++)
	{
		allValue[i] = allValue[i].replace(' USD', '');
		allValue[i] = allValue[i].replace('$', '');
		allValue[i] = allValue[i].replace(',', '');
		total += parseInt(allValue[i]);
	}
	
	total = new Intl.NumberFormat("en-IN", {maximumSignificantDigits: 3}).format(total);
	$("#total").html('$'+total);
}