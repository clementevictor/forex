$(document).ready(function()
{
	var count = 1;
	var answers = ["", "", "", "", "", "", "",];
	
	var question1 = '<p class="p6">¿Cuántos años tienes?</p>'+
					'<p id="answer1" class="p5 check">18 - 25</p>'+
					'<p id="answer2" class="p5 check">26 - 35</p>'+
					'<p id="answer3" class="p5 check">36 - 50</p>'+
					'<p id="answer4" class="p5 check">51 - 60</p>'+
					'<p id="answer5" class="p5 check">+61</p>';
					
	var question2 = '<p class="p6">¿Cuál es el valor actual de sus activos?</p>'+
					'<p id="answer1" class="p5 check">Menos de $25,000</p>'+
					'<p id="answer2" class="p5 check">$25,000 - $75,000</p>'+
					'<p id="answer3" class="p5 check">$75,000 - $150,000</p>'+
					'<p id="answer4" class="p5 check">$150,000 - $300,000</p>'+
					'<p id="answer5" class="p5 check">Más de $300,000</p>';
	
	var question3 = '<p class="p6">¿Por cuánto tiempo piensas invertir?</p>'+
					'<p id="answer1" class="p5 check">5 años</p>'+
					'<p id="answer2" class="p5 check">10 años</p>'+
					'<p id="answer3" class="p5 check">15 años</p>'+
					'<p id="answer4" class="p5 check">20 años</p>'+
					'<p id="answer5" class="p5 check">25 años</p>';
	
	var question4 = '<p id="answer1" class="p6">¿Qué es mas importante para ti?</p>'+
					'<p id="answer2" class="p5-3 check">Minimizar la perdida</p>'+
					'<p id="answer3" class="p5-3 check">Ambos por igual</p>'+
					'<p id="answer4" class="p5-3 check">Maximizar ganancias</p>';
	
	var question5 = '<p class="p6">¿Cuál es la mejor estrategia para usted si el mercado baja?</p>'+
					'<p id="answer1" class="p5-4 check">Vender todo</p>'+
					'<p id="answer2" class="p5-4 check">Vender algunos</p>'+
					'<p id="answer3" class="p5-4 check">Esperar</p>'+
					'<p id="answer4" class="p5-4 check">Comprar más</p>';
	
	var question6 = '<p class="p6">¿De qué manera estás planeando invertir?</p>'+
					'<p id="answer1" class="p5-2 check">Regularmente</p>'+
					'<p id="answer2" class="p5-2 check">Suma global</p>';
	
	var question7 = '<p class="p6">¿Cómo dividirá sus pagos?</p>'+
					'<p id="answer1" class="p5-4 check">Mensualmente</p>'+
					'<p id="answer2" class="p5-4 check">Trimestral</p>'+
					'<p id="answer3" class="p5-4 check">Medio año</p>'+
					'<p id="answer4" class="p5-4 check">Anual</p>';
					
	$(".div6 div").prepend(question1);
	$(".icon-birthday-cake").css("color", "white");
	
	$(".check").click(function()
	{
		answers[0] = $(this).text();
		selectAnswer();
		$(this).css("background", "#1d64e8");
		$(this).css("color", "white");
	});
	
	$(".button1").click(function()
	{
		if($("#submit_email").val() != "")
		{
			x = "&mail="+$("#submit_email").val();
			formulario(x);
		}
		else
		{
			$("#advice").css("display", "flex");
			add = '<p class="error">Favor de ingresar su email</p>';
			$('#advice').html(add);
		}
	});
	
	$(".button2").click(function()
	{
		switch(count)
		{
			case 1: if(answers[0] != "")
					{
						$(".div6 div").html(question2);
						count = 2;
						choosed(count, answers);
						$(".icon-wallet").css("color", "white");
						$(".check").click(function()
						{
							answers[1] = $(this).text();
							selectAnswer();
							$(this).css("background", "#1d64e8");
							$(this).css("color", "white");
						});
					}
					else
					{
						$("#advice").css("display", "flex");
						add = '<p class="error">Favor de seleccionar una opción antes de continuar</p>';
						$('#advice').html(add);
					}
					break;
					
			case 2: if(answers[1] != "")
					{
						$(".div6 div").html(question3);
						count = 3;
						choosed(count, answers);
						$(".icon-clock").css("color", "white");
						$(".check").click(function()
						{
							answers[2] = $(this).text();
							selectAnswer();
							$(this).css("background", "#1d64e8");
							$(this).css("color", "white");
						});
					}
					else
					{
						$("#advice").css("display", "flex");
						add = '<p class="error">Favor de seleccionar una opción antes de continuar</p>';
						$('#advice').html(add);
					}
					break;
					
			case 3: if(answers[2] != "")
					{
						$(".div6 div").html(question4);
						count = 4;
						choosed(count, answers);
						$(".icon-investor").css("color", "white");
						$(".check").click(function()
						{
							answers[3] = $(this).text();
							selectAnswer();
							$(this).css("background", "#1d64e8");
							$(this).css("color", "white");
						});
					}
					else
					{
						$("#advice").css("display", "flex");
						add = '<p class="error">Favor de seleccionar una opción antes de continuar</p>';
						$('#advice').html(add);
					}
					break;
			case 4: if(answers[3] != "")
					{
						$(".div6 div").html(question5);
						count = 5;
						choosed(count, answers);
						$(".icon-growth").css("color", "white");
						$(".check").click(function()
						{
							answers[4] = $(this).text();
							selectAnswer();
							$(this).css("background", "#1d64e8");
							$(this).css("color", "white");
						});
					}
					else
					{
						$("#advice").css("display", "flex");
						add = '<p class="error">Favor de seleccionar una opción antes de continuar</p>';
						$('#advice').html(add);
					}
					break;
			case 5: if(answers[4] != "")
					{
						$(".div6 div").html(question6);
						count = 6;
						choosed(count, answers);
						$(".icon-calendario").css("color", "white");
						$(".check").click(function()
						{
							answers[5] = $(this).text();
							selectAnswer();
							$(this).css("background", "#1d64e8");
							$(this).css("color", "white");
						});
					}
					else
					{
						$("#advice").css("display", "flex");
						add = '<p class="error">Favor de seleccionar una opción antes de continuar</p>';
						$('#advice').html(add);
					}
					break;
			case 6: if(answers[5] != "")
					{
						$(".div6 div").html(question7);
						count = 7;
						choosed(count, answers);
						$(".icon-money-pig").css("color", "white");
						$(".check").click(function()
						{
							answers[6] = $(this).text();
							selectAnswer();
							$(this).css("background", "#1d64e8");
							$(this).css("color", "white");
						});
					}
					else
					{
						$("#advice").css("display", "flex");
						add = '<p class="error">Favor de seleccionar una opción antes de continuar</p>';
						$('#advice').html(add);
					}
					break;
			case 7: if(answers[6] != "")
					{
						$(".div6 div").html(question1);
						count = 1;
						choosed(count, answers);
						$(".icon-birthday-cake").css("color", "white");
						$(".check").click(function()
						{
							answers[0] = $(this).text();
							selectAnswer();
							$(this).css("background", "#1d64e8");
							$(this).css("color", "white");
						});
					}
					else
					{
						$("#advice").css("display", "flex");
						add = '<p class="error">Favor de seleccionar una opción antes de continuar</p>';
						$('#advice').html(add);
					}
					break;
		}
		
	});
	
	$(".icon2").click(function()
	{
		var icon = $(this).attr('class');
		icon = icon.replace(' icon2', '');
		
		switch(icon)
		{
			case 'icon-birthday-cake':  $(".div6 div").html(question1);
										count = 1;
										choosed(count, answers);
										$(".check").click(function()
										{
											answers[0] = $(this).text();
											selectAnswer();
											$(this).css("background", "#1d64e8");
											$(this).css("color", "white");
										});
										break;
										
			case 'icon-wallet': 		if(answers[0] != "")
										{
											$(".div6 div").html(question2);
											count = 2;
											choosed(count, answers);
											$(".check").click(function()
											{
												answers[1] = $(this).text();
												selectAnswer();
												$(this).css("background", "#1d64e8");
												$(this).css("color", "white");
											});
										}
										break;
										
			case 'icon-clock':  		if(answers[1] != "")
										{
											$(".div6 div").html(question3);
											count = 3;
											choosed(count, answers);
											$(".check").click(function()
											{
												answers[2] = $(this).text();
												selectAnswer();
												$(this).css("background", "#1d64e8");
												$(this).css("color", "white");
											});
										}
										break;
										
			case 'icon-investor':  		if(answers[2] != "")
										{
											$(".div6 div").html(question4);
											count = 4;
											choosed(count, answers);
											$(".check").click(function()
											{
												answers[3] = $(this).text();
												selectAnswer();
												$(this).css("background", "#1d64e8");
												$(this).css("color", "white");
											});
										}
										break;
										
			case 'icon-growth':  		if(answers[3] != "")
										{
											$(".div6 div").html(question5);
											count = 5;
											choosed(count, answers);
											$(".check").click(function()
											{
												answers[4] = $(this).text();
												selectAnswer();
												$(this).css("background", "#1d64e8");
												$(this).css("color", "white");
											});
										}
										break;
			case 'icon-calendario':  	if(answers[4] != "")
										{
											$(".div6 div").html(question6);
											count = 6;
											choosed(count, answers);
											$(".check").click(function()
											{
												answers[5] = $(this).text();
												selectAnswer();
												$(this).css("background", "#1d64e8");
												$(this).css("color", "white");
											});
										}
										break;
			case 'icon-money-pig':  	if(answers[5] != "")
										{
											$(".div6 div").html(question7);
											count = 7;
											choosed(count, answers);
											$(".check").click(function()
											{
												answers[6] = $(this).text();
												selectAnswer();
												$(this).css("background", "#1d64e8");
												$(this).css("color", "white");
											});
										}
										break;
		}
	});
});

function selectAnswer()
{
	$(".check").css("background", "white");
	$(".check").css("color", "black");
}

function choosed(count, answers)
{
	var allText = $(".check").map(function(){
               return $.trim($(this).text());
            }).get();
			
	var allId = $(".check").map(function(){
               return $.trim($(this).attr("id"));
            }).get();
	
	for(i=0;i<allText.length;i++)
	{
		switch(count)
		{
			case 1: if(allText[i] == answers[0])
					{
						$("#"+allId[i]).css("background", "#1d64e8");
						$("#"+allId[i]).css("color", "white");
					}
					break;
			case 2: if(allText[i] == answers[1])
					{
						$("#"+allId[i]).css("background", "#1d64e8");
						$("#"+allId[i]).css("color", "white");
					}
					break;
			case 3: if(allText[i] == answers[2])
					{
						$("#"+allId[i]).css("background", "#1d64e8");
						$("#"+allId[i]).css("color", "white");
					}
					break;
			case 4: if(allText[i] == answers[3])
					{
						$("#"+allId[i]).css("background", "#1d64e8");
						$("#"+allId[i]).css("color", "white");
					}
					break;
			case 5: if(allText[i] == answers[4])
					{
						$("#"+allId[i]).css("background", "#1d64e8");
						$("#"+allId[i]).css("color", "white");
					}
					break;
			case 6: if(allText[i] == answers[5])
					{
						$("#"+allId[i]).css("background", "#1d64e8");
						$("#"+allId[i]).css("color", "white");
					}
					break;
			case 7: if(allText[i] == answers[6])
					{
						$("#"+allId[i]).css("background", "#1d64e8");
						$("#"+allId[i]).css("color", "white");
					}
					break;
		}
		
	}
}