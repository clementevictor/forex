$(document).ready(function()
{
	var add = '<option value="">MM</option>';
	var iconVisa = '<span class="icon-visa icon2 visa"><span class="path1"></span><span class="path2"></span></span>';
	var iconMaster = '<span class="icon-mastercard icon2 visa"><span class="path1"></span><span class="path2"></span></span>';
	var iconAmex = '<span class="icon-amex icon2 amex"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span></span>';
	var iconNormal = '<span class="icon-credit-card icon2"></span>';
	var b1 = 0;
	var b2 = 0;
	var b3 = 0;
	var correct = [2];
	var arrayDataCourses = JSON.parse($.cookie('course'));
	var arrayDataPrices = JSON.parse($.cookie('price'));
	var date = $.cookie('date').replace('     '+hour, '');
	var hour = $.cookie('date').split("     ").pop();
	var totalCookie = $.cookie('total');

	


	constructPage(arrayDataCourses, arrayDataPrices, add);
	
	document.getElementById('credit_card').addEventListener('input', function (e)
	{
		e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
	});
	
	$("#course1").html(arrayDataCourses[0]);
	$("#course2").html(arrayDataCourses[1]);
	$("#price1").html(arrayDataPrices[0]);
	$("#price2").html(arrayDataPrices[1]);
	$("#hour").html(tConvert(hour));
	$("#date").html(convertDate(date));
	$("#modal_total").html(totalCookie+" usd");

	$("#tp_id").change(function()
	{
		check = $(this).val().replace(/\s/g,'');
		if($.isNumeric(check) && $(this).val().length == 10)
		{
			$(".p55").css("border", "solid 2px #2e9c46");
		}
		else
		{
			$(".p55").css("border", "solid 2px #e45050");
		}
	});
	
	$("#credit_card").keyup(function()
	{
		var check = $(this).val();
		check = check.charAt(0);
		switch(check)
		{
			case '4':  $(".icon2").remove(); $(".p5").append(iconVisa); b1 = 1; break;
			case '5':  $(".icon2").remove(); $(".p5").append(iconMaster); b1 = 2; break;
			case '3':  $(".icon2").remove(); $(".p5").append(iconAmex); b1 = 3; break;
			default:	$(".icon2").remove();
						$(".p5").append(iconNormal);
						b1 = 0;
						$(this).css("width", "85%");
						$("#incorrect1").remove();
						add = '<span class="incorrect" id="incorrect1">Tu tarjeta es inválida, intentalo de nuevo</span>';
						$(".p5").after(add);
						$(".p5").css("border", "solid 2px #e45050");
						correct[0] = 0;
						checkForm(correct);
						break;
		}
		
		if($(this).val().length == 19)
		{
			check = $(this).val().replace(/\s/g,'');
			if($.isNumeric(check) && b1 != 0)
			{
				$("#correct_card").remove();
				$("#incorrect1").remove();
				add = '<span class="icon-correct correct_card" id="correct_card"></span>';
				$(this).after(add);
				
				if(b1 == 3) $(this).css("width", "70%");
				else $(this).css("width", "81%");
				
				$(".p5").css("border", "solid 2px #2e9c46");
				correct[0] = 1;
				checkForm(correct);
			}
		}
	});
	
	$("#month").change(function()
	{
		check = $(this).val().replace(/\s/g,'');
		if($(this).val() > 0 && $(this).val() < 13 && $.isNumeric(check))
		{
			b2 = 1;
			if(b2 == 1 && b3 == 1)correctMY(correct);
		}
		else
		{
			b2 = 0;
			$("#incorrect2").remove();
			add = '<span class="incorrect" id="incorrect2">Ingresa una fecha válida</span>';
			$(".p8").after(add);
			$(".p6").css("border", "solid 2px #e45050");
			correct[1] = 0;
			checkForm(correct);
		}
	});
	
	$("#year").change(function()
	{
		check = $(this).val().replace(/\s/g,'');
		year = new Date().getFullYear();
		year = year.toString().slice(2, 3);
		if($(this).val() >= year && $.isNumeric(check))
		{
			b3 = 1;
			if(b2 == 1 && b3 == 1)correctMY(correct);
		}
		else
		{
			b3 = 0;
			$("#incorrect2").remove();
			$("#correct_month").remove();
			add = '<span class="incorrect" id="incorrect2">Ingresa una fecha válida</span>';
			$(".p8").after(add);
			$(".p6").css("border", "solid 2px #e45050");
			correct[1] = 0;
			checkForm(correct);
		}
	});
	
	$("#cvv").change(function()
	{
		check = $(this).val().replace(/\s/g,'');
		if($.isNumeric(check) && $(this).val().length == 3)
		{
			$(".p8").css("border", "solid 2px #2e9c46");
			correct[2] = 1;
			$("#correct_cvv").remove();
			add = '<span class="icon-correct correct_card" id="correct_cvv"></span>';
			$("#cvv").after(add);
			$("#correct_cvv").css("margin-right", "12%");
			checkForm(correct);
		}
		else
		{
			$(".p8").css("border", "solid 2px #e45050");
			correct[2] = 0;
			checkForm(correct);
		}
	});
	
	$("#credit_card").keyup(function(e)
	{
		if(e.keyCode == 8)
		{
			$("#correct_card").remove();
			$("#incorrect1").remove();
			correct[0] = 0;
			add = '<span class="incorrect" id="incorrect1">Tu tarjeta es inválida, intentalo de nuevo</span>';
			$(".p5").after(add);
			$(".p5").css("border", "solid 2px #e45050");
			checkForm(correct);
		}
	});
	
	$("#advice_buy_close").click(function()
	{
		$(".advice_buy").css("display", "none");
	});
});

function constructPage(arrayDataCourses, arrayDataPrices, add)
{
	var table = '<table id="table_shopping">';
	
	for(i=0;i<arrayDataCourses.length;i++)
	{
		if(arrayDataCourses[i] != "")
			table += '<tr id="tr'+i+'"><td class="td1">'+arrayDataCourses[i]+'</td><td class="td2">'+arrayDataPrices[i]+'</td><td class="td3"><label class="icon-trash" id="trash'+i+'"></label></td></tr>';
	}
					
	table += '</table>'+
				'<p class="p12">TOTAL <span class="total">'+$.cookie('total')+'</span> <label>USD</label></p>';
	
	$(".total").html($.cookie('total'));
	$(".div2").append(table);
	$(".div1 .p3:nth-child(2)").css("color", "#d2d2d2");
	$(".div1 .p3:nth-child(2) span").css("background", "#d2d2d2");
	$(".div1 .p4:nth-child(1)").css("color", "white");
	$(".div1 .p4:nth-child(1)").css("background", "#f0923c");
	
	for(var o=0;o<arrayDataCourses.length;o++)
	{
		if(arrayDataCourses[o] != "")
		{
			createEvent(o);
		}
	}
	
	for(i=1;i<=12;i++)
	{
		add += '<option value="'+i+'">'+i+'</option>';
	}
	
	$(".select1").html(add);
}



function changePSP(psp){
	
	$(".div1 .p4:nth-child(1)").css("color", "black");
	$(".div1 .p4:nth-child(1)").css("background", "white");

	$(".div1 .p4:nth-child(2)").css("color", "black");
	$(".div1 .p4:nth-child(2)").css("background", "white");

	$(".div1 .p4:nth-child(3)").css("color", "black");
	$(".div1 .p4:nth-child(3)").css("background", "white");

	switch(psp) {
	  case 1:
	    $(".div1 .p4:nth-child(1)").css("color", "white");
		$(".div1 .p4:nth-child(1)").css("background", "#f0923c");
		$("#psp").val(1);
	    console.log(psp);
	    break;
	  case 2:
	    $(".div1 .p4:nth-child(2)").css("color", "white");
		$(".div1 .p4:nth-child(2)").css("background", "#f0923c");
		$("#psp").val(2);
		console.log(psp);
	    break;
	  case 3:
	    // $(".div1 .p4:nth-child(3)").css("color", "white");
		// $(".div1 .p4:nth-child(3)").css("background", "#f0923c");
	    // psp = 3;
	    alert("No disponible");
	    $(".div1 .p4:nth-child(1)").css("color", "white");
		$(".div1 .p4:nth-child(1)").css("background", "#f0923c");
		$("#psp").val(1);
	    break;

	  default:
	    // code block
	}
}


function createEvent(o)
{
	$("#trash"+o).click(function()
	{
		var newTotal = $("#tr"+o+" .td2").text();
		var oldTotal = $(".p2 .total").text();
		newTotal = newTotal.replace(' USD', '');
		newTotal = newTotal.replace('$', '');
		newTotal = newTotal.replace(',', '');
		newTotal = parseInt(newTotal);
		oldTotal = oldTotal.replace('$', '');
		oldTotal = oldTotal.replace(',', '');
		oldTotal = parseInt(oldTotal);
		newTotal = oldTotal - newTotal;
		$("#tr"+o).remove();
		newTotal = new Intl.NumberFormat("en-IN", {maximumSignificantDigits: 3}).format(newTotal);
		$(".total").html("$"+newTotal);
	});
}

function correctMY(correct)
{
	$("#correct_month").remove();
	$("#incorrect2").remove();
	add = '<span class="icon-correct correct_card" id="correct_month"></span>';
	$("#year").after(add);
	$(".p6").css("border", "solid 2px #2e9c46");
	correct[1] = 1;
	checkForm(correct);
}




function checkForm(correct)
{
	if(correct[0] == 1 && correct[1] == 1 && correct[2] == 1 && $(".p2 .total").text() != "$0")
	{
		var transformCourse = "";
		$(".button1").css("background", "#f0923c");
		$(".button1").click(function()
		{
			
			var allCourse = $(".td1").map(function(){
				   return $.trim($(this).text());
				}).get();
			var allPrice = $(".td2").map(function(){
				   return $.trim($(this).text());
				}).get();
			
			var amount = 0;

			for(i=0;i<allCourse.length;i++)
			{
				allPrice[i] = allPrice[i].substr(1);
				allPrice[i] = allPrice[i].replace(",","");
				allPrice[i] = parseInt(allPrice[i]);

				amount = amount + allPrice[i]; 
				transformCourse += "&course"+i+"="+allCourse[i]+"&price"+i+"=";

			}

			var form = $("#form_payment").serialize()+"&origin=pago"+transformCourse;
			

			switch($("#psp").val()) {
			  case "1":
			    conekta(form);
			    break;
			  case "2":
			    feenicia(form);
			    break;
			  default:
			    // code block
			}

			
		});
	}
	else
	{
		$(".button1").remove();
		add = '<button class="button1">Comprar ahora</button>';
		$(".aside1").append(add);
	}
}

function tConvert(time)
{
	// Check correct time format and split into components
	time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

	if (time.length > 1)
	{ // If time format correct
		time = time.slice (1);  // Remove full string match value
		time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
		time[0] = +time[0] % 12 || 12; // Adjust hours
	}
	return time.join (''); // return adjusted time or original string
}

function convertDate(date)
{
	var d = new Date(date);
	var weekday = new Array(7);
	var month = new Array(12);
	weekday[0] = "Domingo";
	weekday[1] = "Lunes";
	weekday[2] = "Martes";
	weekday[3] = "Miércoles";
	weekday[4] = "Jueves";
	weekday[5] = "Viernes";
	weekday[6] = "Sábado";
	
	month[0] = "Enero";
	month[1] = "Febrero";
	month[2] = "Marzo";
	month[3] = "Abril";
	month[4] = "Mayo";
	month[5] = "Junio";
	month[6] = "Julio";
	month[7] = "Agosto";
	month[8] = "Septiembre";
	month[9] = "Octubre";
	month[10] = "Noviembre";
	month[11] = "Diciembre";

	return weekday[d.getDay()]+" "+date.slice(-2)+" de "+month[d.getMonth()]+" "+d.getFullYear();
}

function conekta(form){

	var amount = $.cookie('total').substr(1);
	amount = parseFloat(amount.replace(/,/g, ''));
	console.log(amount);

	var tp_id = $("#tp_id").val(); 
	
	$.post("https://www.orotrader.com/paysys/api/core.php",{
		step: "1",
		tp_id: tp_id ,
		data: {	adress: "Versalles 12",
				currency: "250",
				amount: amount
			}
	},
		function(data, status){
		
		// console.log("Data: " + JSON.stringify(data) + "\nStatus: " + status);
	
		if (status == "success"){
			var $form = $("#form_payment");
			var lambda = data["token"];
			var email = data.content.user_data.email;
			var name = data.content.user_data.first_name + " " + data.content.user_data.last_name;
			var phone = data.content.user_data.phone_code+data.content.user_data.phone_number
			$("#name").val(data.content.user_data.first_name + " " + data.content.user_data.last_name);
			    	

			// ORO TRADER PASO 7
			var formdata2 = new FormData();
			formdata2.append("step", "7");
		    formdata2.append("tp_id", tp_id);
		    formdata2.append("data",
		              		btoa(
		                		btoa(
		                  		JSON.stringify({
		                    		currency: "USD",
		                    		amount: amount,
		                    		selectedMeth: { idpsp: "28" },
		                    		email: email,
		                  		})
		                		) + lambda
		              		)
		    );
			    	

			fetch("https://www.orotrader.com/paysys/api/core.php", 
				{
					method: "POST",
				    body: formdata2,
				})
				.then((res) => res.json())
				.then(function(response) {

			    	var orderid = response["data"];

					if (status == "success"){
					    
					    //script conekta
						Conekta.setPublicKey('key_Eb2s33QTaAhdcKpJqqLkUJQ');

						var conektaSuccessResponseHandler = function(token){
							console.log("paso por aqui Response Handler");
								
							//Inserta el token_id en la forma para que se envíe al servidor
							$form.append($('<input type="hidden" name="conektaTokenId" id="conektaTokenId">').val(token.id));
							console.log(token.id);
							// x = "&"+$("#form_payment").serialize()+"&origin=pago"+transformCourse;

							$.post("api/conekta/integracion.php",
								{
									conektaTokenId: token.id,
									amount: amount,
									name:   name,
									phone:  phone,
									email:  email
								},
							function(data,status,result){	

								// console.log(data);
								// console.log(status);
								// console.log(result);
								if (data == "") {
									console.log("COMPLETE");
			                        response_status = "COMPLETE";
			                            
			                    } else {
			                        $form.find(".card-errors").text(JSON.stringify(data));
			                       	console.log("REJECTED");
			                        response_status = "REJECTED";
			                    }

			                    fetch(
		                           	"https://www.orotrader.com/paysys/api/callback/conektaCallBack.php?order-id=" +
		                            orderid +
		                            "&status=" +
		                            response_status,
		                           	{
		                            method: "GET",
		                            }
		                          	)
		                            	// .then((res) => res.json())
		                            	.then((result) => {
		                              	console.log(result);
		                            	});
								});
							};
						var conektaErrorResponseHandler = function(response){
								console.log("paso por aqui Response ERROR Handler");
								$form.find(".card-errors").text(response.message + " " + response.message_to_purchaser);
								$form.find("button").prop("disabled", false);
						};

							
						var $form = $("#form_payment");
						// Previene hacer submit más de una vez
						// $(".button1").remove();

						//$form.find("button1").prop("disabled", true);


						Conekta.Token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);
					}
				})
			}
		});

}

function feenicia(form){
	// console.log("Entro Feenicia");


	var signatureIV = CryptoJS.enc.Hex.parse("a2a10442bb47a0335ee659aaf1be5f83");
	var signatureKEY = CryptoJS.enc.Hex.parse("1bc8a97e67b2495960994afabfe9b48f");
	var requestIv = CryptoJS.enc.Hex.parse("973891a619425d6b3ba1e990b43a8d97");
	var requestKey = CryptoJS.enc.Hex.parse("087e50ec2bdec4d6c5cba801cf2e06ca");
	var merchant = '0000000000006038';
	var tp_id = $("#tp_id").val();
	var amount = $.cookie('total').substr(1);
	amount = parseFloat(amount.replace(/,/g, ''));

	var tp_id= tp_id;
	var cardholderName = 'Pruebas';
	var cvv2 = '123';
	var expDate = '2510';
	var pan = '44445555666779';
	var unitPrice = amount;
	var description = "Soy un Producto"
	
	cardholderName = CryptoJS.enc.Hex.stringify(CryptoJS.AES.encrypt(cardholderName, requestKey, {iv: requestIv}).ciphertext);
	cvv2 = CryptoJS.enc.Hex.stringify(CryptoJS.AES.encrypt(cvv2, requestKey, {iv: requestIv}).ciphertext);
	expDate = CryptoJS.enc.Hex.stringify(CryptoJS.AES.encrypt(expDate, requestKey, {iv: requestIv}).ciphertext);
	pan = CryptoJS.enc.Hex.stringify(CryptoJS.AES.encrypt(pan, requestKey, {iv: requestIv}).ciphertext);

	var formData = new FormData();
	formData.append('tp_id', tp_id);
	formData.append('cardholderName', cardholderName );
	formData.append('cvv2', cvv2);
	formData.append('expDate', expDate);
	formData.append('pan', pan);
	formData.append('unitPrice', unitPrice);
	formData.append('description', description);



	var jsonRequest= '{"amount": 1,"items": [{"amount": 1,"description": "'+description+'","Id": 0,"Quantity": "1","unitPrice": "'+unitPrice+'"}],"merchant": "0000000000006038","userId": "armando385"}';



	var sha256 = CryptoJS.SHA256(jsonRequest);
	var aes = CryptoJS.AES.encrypt(CryptoJS.enc.Hex.stringify(sha256), signatureKEY, {iv: signatureIV});

	var headerToken = merchant+"_"+CryptoJS.enc.Hex.stringify(aes.ciphertext);
	console.log(jsonRequest);
	// console.log("HOLA SOY EL TOKEN YA ENCRYPTADO");
	console.log(headerToken);
	
	// throw new Error(headerToken);

	formData.append('headerToken', headerToken);

	//OROTRADER PASO 1

	$.post("https://www.orotrader.com/paysys/api/core.php",{
	  	step: "1",
	    tp_id: tp_id ,
	    data: {	adress: "Versalles 12",
				currency: "USD",
				amount: unitPrice}
	},
	function(data, status){
	  	console.log("Data: " + JSON.stringify(data) + "\nStatus: " + status);

	  	if (data.error == "0"){
	  	var lambda = data["token"];
	    var email = data.content.user_data.email;
	    var name = data.content.user_data.first_name + " " + data.content.user_data.last_name;
	    var phone = data.content.user_data.phone_code+data.content.user_data.phone_number

	    	

	   	// ORO TRADER PASO 7
	    var formdata2 = new FormData();
	    formdata2.append("step", "7");
        formdata2.append("tp_id", tp_id);
        formdata2.append(
        	"data",
            	btoa(
             		btoa(
                  		JSON.stringify({
                    	currency: "USD",
                    	amount: unitPrice,
                    	selectedMeth: { idpsp: "29" },
                    	email: email,
                  	})
                	) + lambda
              	)
            );
        fetch("https://www.orotrader.com/paysys/api/core.php", {
		              method: "POST",
		              body: formdata2,
		     })
		    	.then((res) => res.json())
		    	.then(function(response) {
		    		var orderidOroTrader = response["data"];

		    		fetch("api/feenicia/binding.php", {
		            	method: "POST",
		            	body: formData,
		    		})
		    			.then((res) => res.json())
		    			.then(function(response) {
		    				// OBTENEMOS EL ID
		    				// console.log(response.orderId);
		    				if (response.orderId != 0 ){
		    					orderid = response.orderId;
		    					console.log("orderId: " + orderid);
		    					
		    					var transactionDate = Date.now();
		    					console.log(transactionDate);

		    					jsonRequest = '{"affiliation":"9165714","amount":1,"transactionDate":'+ transactionDate+',"orderId":'+orderid+',"tip":0,"pan":"'+pan+'","cardholderName":"'+cardholderName+'","cvv2":"'+cvv2+'","expDate":"'+expDate+'"}';
		    					
		    					// console.log(jsonRequest);
		    					// throw new Error("ESTE FUE EL JSON");
					    		var sha256 = CryptoJS.SHA256(jsonRequest);
								var aes = CryptoJS.AES.encrypt(CryptoJS.enc.Hex.stringify(sha256), signatureKEY, {iv: signatureIV});

								var headerToken = merchant+"_"+CryptoJS.enc.Hex.stringify(aes.ciphertext);

								formData.set('headerToken', headerToken);
		    					

								var settings = {
												  "url": "https://www.feenicia.net/v1/atna/sale/manual",
												  "method": "POST",
												  "timeout": 0,
												  "headers": {
												    "Content-Type": "application/json",
												    "x-requested-with": headerToken
												  },
												  "data": JSON.stringify({"affiliation":"9165714","amount":1,"transactionDate":transactionDate,"orderId":orderid,"tip":0,"pan":pan,"cardholderName": cardholderName,"cvv2":cvv2,"expDate":expDate}),
												  success: function (response) {
												  	response_status = "COMPLETE";
											        alert("Se logro el pago");
											      },
												  error: function (xhr, ajaxOptions, thrownError) {
												  	response_status = "REJECTED";
													  	alert("hubo un error");
													  	fetch(
						                            "https://www.orotrader.com/paysys/api/callback/conektaCallBack.php?order-id=" +
						                              orderidOroTrader +
						                              "&status=" +
						                              response_status,
						                            {
						                              method: "GET",
						                            })
						                            // .then((res) => res.json())
						                            .then((result) => {
						                              console.log(result);
						                            });
													  }
												};

								$.ajax(settings).done(function (response) {
									// console.log(response);
									fetch(
		                            "https://www.orotrader.com/paysys/api/callback/conektaCallBack.php?order-id=" +
		                              orderidOroTrader +
		                              "&status=" +
		                              response_status,
		                            {
		                              method: "GET",
		                            })
		                            .then((res) => res.json())
		                            .then((result) => {
		                              console.log(result);
		                            });

									
								});


		    				} else {
		    					console.log("Error en el pago");
		    				}


		    			});
		    	});



	  } else { //No Funciono el paso 1.
	  	console.log("Error: " + data.error + " " +data.desc);
	  }
	});
}