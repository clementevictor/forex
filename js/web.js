var header = '<header>'+
				'<a href="index.html"><img src="img/logo.png"></a>'+
				'<span class="icon-menu movil"></span>'+
				'<ul>'+
					'<li><a href="Webinars.html">Webinars</a></li>'+
					'<li><a href="Quienes-somos.html">Quienes somos</a></li>'+
					'<li><a>Expertos</a></li>'+
					'<li id="login">Login</li>'+
					'<li><button id="header_send">Registrate</button></li>'+
					'<li class="movil close-menu"><label class="icon-close" id="menu_close"></label></li>'+
				'</ul>'+
			'</header>'+
			'<div id="advice"></div>'+
			'<div class="modal_form">'+
				'<div>'+
					'<label class="icon-close" id="modal_close"></label>'+
					'<p class="modal_p1">Inscríbete y comienza a aprender</p>'+
					'<form id="modal_form">'+
						'<input type="text" placeholder="Nombre" class="modal_text1" id="modal_name"/>'+
						'<input type="text" placeholder="Correo electrónico" class="modal_text1" id="modal_mail"/>'+
						'<input type="password" placeholder="Constraseña" class="modal_text1" id="modal_pass"/>'+
					'</form>'+
					'<button id="modal_send">Registrate</button>'+
					'<p class="modal_p2">¿Ya tienes una cuenta? <a>Inicia sesión</a></p>'+
				'</div>'+
			'</div>'+
			'<div class="modal_login">'+
				'<div>'+
					'<label class="icon-close" id="modal_login_close"></label>'+
					'<p class="modal_p1">Entra y comienza a aprender</p>'+
					'<form id="login_form">'+
						'<input type="text" placeholder="Correo electrónico" class="modal_text1" id="modal_mail_login"/>'+
						'<input type="password" placeholder="Constraseña" class="modal_text1" id="modal_pass_login"/>'+
					'</form>'+
					'<button id="modal_login_send">Ingresa</button>'+
					'<p class="modal_p2">¿Ya tienes una cuenta? <a>Inicia sesión</a></p>'+
				'</div>'+
			'</div>';
			
var footer = '<footer>'+
				'<aside class="footer_aside1">'+
					'<p class="footer_p4">Nos pondremos<br>en contacto<br>contigo</p>'+
					'<div>'+
						'<p class="footer_p3">Contáctenos</p>'+
						'<form id="footer_form">'+
							'<input type="text" placeholder="Nombre" class="footer_text1" id="footer_name"/>'+
							'<input type="number" placeholder="Teléfono / celular" class="footer_text1" id="footer_phone"/>'+
							'<input type="text" placeholder="Email" class="footer_text1" id="footer_mail"/>'+
						'</form>'+
						'<button id="footer_send">Enviar</button>'+
					'</div>'+
				'</aside>'+
				'<aside class="footer_aside2">'+
					'<p class="footer_p1">Menú</p>'+
					'<a href="Webinars.html">Webinars</a>'+
					'<a href="Quienes-somos.html">Quienes somos</a>'+
					'<a>Expertos</a>'+
					'<a>Inscribete</a>'+
					'<a>Registro</a>'+
				'</aside>'+
				'<p class="footer_p2">Aviso de privacidad</p>'+
				'<p class="footer_p5">Terminos y condiciones</p>'+
			'</footer>';
			
$(document).ready(function()
{
	$("body").prepend(header);
	$("body").append(footer);
	var add = "";
	
	$('.icon-menu').click(function()
	{
		$("header ul").css("right", "0%");
		$("header").css("padding", "81% 8.5%");
	});
	
	$('#menu_close').click(function()
	{
		$("header ul").css("right", "-100%");
		$("header").css("padding", "0.5% 8.5%");
	});
	
	$('#login').click(function()
	{
		$(".modal_login").css("top", "0%");
	});
	
	$('#footer_send').click(function()
	{
        if($('#footer_name').val() == "" || $('#footer_phone').val() == "" || $('#footer_mail').val() == "" )
		{
			$("#advice").css("display", "flex");
            add = '<p class="error">Favor de llenar todos los campos</p>';
			$('#advice').html(add);
			
			setTimeout(function()
            {
                $("#advice").css("display", "none");
            }, 2000);
        }
		else if(!$('#footer_mail').val().includes("@") || !$('#footer_mail').val().includes("."))
		{
			$("#advice").css("display", "flex");
            add = '<p class="error">El correo no es valido</p>';
			$('#advice').html(add);
			
			setTimeout(function()
            {
                $("#advice").css("display", "none");
            }, 2000);
		}
		else
		{
			x = "&"+$("#footer_form").serialize()+"&origin=footer";
			formulario(x);
		}
    });
	
	$('#modal_send').click(function()
	{
        if($('#modal_name').val() == "" || $('#modal_mail').val() == "" || $('#modal_pass').val() == "" )
		{
			$("#advice").css("display", "flex");
            add = '<p class="error">Favor de llenar todos los campos</p>';
			$('#advice').html(add);
			
			setTimeout(function()
            {
                $("#advice").css("display", "none");
            }, 2000);
        }
		else if(!$('#modal_mail').val().includes("@") || !$('#modal_mail').val().includes("."))
		{
			$("#advice").css("display", "flex");
            add = '<p class="error">El correo no es valido</p>';
			$('#advice').html(add);
			
			setTimeout(function()
            {
                $("#advice").css("display", "none");
            }, 2000);
		}
		else
		{
			x = "&"+$("#modal_form").serialize()+"&origin=modal";
			formulario(x);
		}
    });
	
	$("#modal_login_send").click(function()
	{
        if($('#modal_mail_login').val() == "" || $('#modal_pass_login').val() == "")
		{
			$("#advice").css("display", "flex");
            add = '<p class="error">Favor de llenar todos los campos</p>';
			$('#advice').html(add);
			
			setTimeout(function()
            {
                $("#advice").css("display", "none");
            }, 2000);
        }
		else if(!$('#modal_mail_login').val().includes("@") || !$('#modal_mail_login').val().includes("."))
		{
			$("#advice").css("display", "flex");
            add = '<p class="error">El correo no es valido</p>';
			$('#advice').html(add);
			
			setTimeout(function()
            {
                $("#advice").css("display", "none");
            }, 2000);
		}
		else
		{
			x = "&"+$("#login_form").serialize()+"&origin=modal_login";
			formulario(x);
		}
	});
	
	$("#advice").click(function()
	{
		$("#advice").css("display", "none");
	});
	
	$('#header_send, .index_button3, .who_button2').click(function()
	{
		$(".modal_form").css("top", "0%");
	});
	
	$('#modal_close').click(function()
	{
		$(".modal_form").css("top", "-100%");
	});
	
	$('#modal_login_close').click(function()
	{
		$(".modal_login").css("top", "-100%");
	});
});

function formulario(x)
{
	$.post("conekta/integracion.php",
	{
		conektaTokenId: x,
	},
	function(data, status)
	{
		console.log("Data: " + data + "\nStatus: " + status);
	});
}

